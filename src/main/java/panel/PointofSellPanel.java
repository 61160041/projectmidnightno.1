/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import Piyachat_61160263.Change;
import Piyachat_61160263.OrderDetail;
import Piyachat_61160263.OrderPos;
import Piyachat_61160263.ProductImage;
import dao.ProductDao;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import Piyachat_61160263.OrderList;
import dao.CustomerDao;
import dao.OrderDao;
import java.text.SimpleDateFormat;
import java.util.Date;
import model.Customer;
import model.Order;
import model.User;

/**
 *
 * @author Windows10
 */
public class PointofSellPanel extends javax.swing.JPanel implements ListProductPanel.OnBuyProductListener {

    private ArrayList<ProductImage> productList;
    private ListTableModel model;
    private OrderPos list;
    private ArrayList<Customer> customerList;
    private double discount = 0;
    private String member = null;
    private Order order;
    private ArrayList<OrderDetail> orderList;
    private ArrayList<Change> cashList;
    private ArrayList<Order> orderList2;
    private User user;
    private ArrayList<Order> orderList1;

    /**
     * Creates new form pos
     */
    public PointofSellPanel(User user) {
        initComponents();
        loadProduct();
        initForm();
        this.user = user;
    }

    public void initForm() {
        llblBill.setVisible(false);
        tblList.revalidate();
        lblName.setVisible(false);
        lblList.setVisible(false);
        lblPrice.setVisible(false);
        lblTotal.setVisible(false);
        lblCash.setVisible(false);
        lblchange.setVisible(false);
        btnCal.setVisible(false);
        inputCash.setVisible(false);
        Jtbl.setVisible(false);
        lblAmount.setVisible(false);
        btnBill.setVisible(false);
        lblSpaceAmount.setText("");
        lblSpaceCash.setText("");
        lblSpaceChange.setText("");
        lblSpaceTotal.setText("");
        lblDiscount.setVisible(false);
        lblSpaceDiscount.setText("");
        inputMember.setVisible(false);
        btnMember.setVisible(false);
        lblIncash.setVisible(false);
        scrReceipt.setVisible(false);

    }

    private void loadProduct() {
        discount = 0;
        posPanel.removeAll();
        posPanel.revalidate();
        ProductDao dao = new ProductDao();
        productList = dao.getAllpic();
        int productSize = productList.size();
        posPanel.setLayout(new GridLayout(productSize / 2 + productSize % 2, 2));
        for (ProductImage product : productList) {
            ListProductPanel p = new ListProductPanel(product);
            p.addOnBuyProductListener(this);
            posPanel.add(p);
        }
    }

    public void loadTable() {
        llblBill.setVisible(true);
        lblName.setVisible(true);
        lblList.setVisible(true);
        lblPrice.setVisible(true);
        lblTotal.setVisible(true);
        lblCash.setVisible(true);
        lblchange.setVisible(true);
        btnCal.setVisible(true);
        inputCash.setVisible(true);
        Jtbl.setVisible(true);
        lblAmount.setVisible(true);
        btnBill.setVisible(true);
        lblDiscount.setVisible(true);
        lblSpaceDiscount.setText("-");
        inputMember.setVisible(true);
        btnMember.setVisible(true);
        lblIncash.setVisible(true);
        scrReceipt.setVisible(true);
        model = new ListTableModel(list.getData());
        tblList.setModel(model);
    }

    public void getType(String type) {
        posPanel.removeAll();
        posPanel.revalidate();
        ProductDao dao = new ProductDao();
        productList = dao.getType(type);
        System.out.println(productList.size());
        int productSize = productList.size();
        posPanel.setLayout(new GridLayout(productSize / 2 + productSize % 2, 2));
        for (ProductImage product : productList) {
            ListProductPanel p = new ListProductPanel(product);
            p.addOnBuyProductListener(this);
            posPanel.add(p);
        }
    }

    private void loadTotal() {
        if (list != null) {
            lblSpaceTotal.setText("" + list.Total());
            lblSpaceAmount.setText("" + list.allAmount());
        }
    }

    public void cal() {
        checkMember();
        double cash = Double.parseDouble(inputCash.getText());
        Change c = new Change(0,0,0);
        c.setChange(cash);
        double total = Double.parseDouble("" + list.Total());
        cash = cash + discount;
        double change = cash - total;
        c.setCash(change);
        OrderDao dao = new OrderDao();
        dao.add(c);
        if (change >= 0) {
            lblSpaceChange.setText("" + change);
        } else {
            lblSpaceChange.setText("money not enough");
        }
        list.resetAmount();
    }

    public void refershTable() {
        tblList.revalidate();
        tblList.repaint();
    }

    private void checkMember() {
        member = inputMember.getText();
        CustomerDao dao = new CustomerDao();
        customerList = dao.getAll();
        if (!member.equals("")) {
            for (int i = 0; i < customerList.size(); i++) {
                if (member.equals(customerList.get(i).getName())) {
                    if (customerList.get(i).getStatus().equals("member")) {
                        discount();
                        lblSpaceDiscount.setText("" + discount);
                        System.out.println(customerList.get(i).getName());
                        return;
                    } else {
                        resetDiscount();
                        lblSpaceDiscount.setText("not member");
                    }
                }
            }
        } else if (member.equals("")) {
            resetDiscount();
            lblSpaceDiscount.setText("-");
        }
    }

    private double resetDiscount() {
        discount = 0;
        return discount;
    }

    private double discount() {
        discount = 15;
        return discount;
    }

    public void addOrderToFrom() {
        order = new Order(0, 0, 0, "", 0.0, "");
        double total = Double.parseDouble("" + list.Total());
        OrderDao daoOrder = new OrderDao();
        member = inputMember.getText();
        CustomerDao daoCus = new CustomerDao();
        customerList = daoCus.getAll();
        if (!member.equals("")) {
            for (int i = 0; i < customerList.size(); i++) {
                if (member.equals(customerList.get(i).getName())) {
                    if (customerList.get(i).getStatus().equals("member")) {
                        order.setCus_id(customerList.get(i).getId());
                        order.setName(member);
                        order.setPrice(total - discount);
                    } else {
                        order.setPrice(total);
                    }
                }
            }
        }
        order.setEmp_id(this.user.getEmpID());
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String strDate = formatter.format(date);
        order.setDate(strDate);
        System.out.println(order.getDate());
        daoOrder.add(order);

    }

    private void updateIdOrderList() {
        OrderDao dao = new OrderDao();
        orderList = dao.getAllList();
        orderList2 = dao.getAll();
        int ordersize = orderList2.size() - 1;
        for (int i = 0; i < orderList.size(); i++) {
            if (orderList.get(i).getOrderId() == 0) {
                OrderDetail listOrder = new OrderDetail(0, 0, 0, 0.0, 0.0);
                listOrder.setOrderId(orderList2.get(ordersize).getId());
                listOrder.setId(orderList.get(i).getId());
                listOrder.setProId(orderList.get(i).getProId());
                listOrder.setTotal(orderList.get(i).getTotal());
                listOrder.setUnitPrice(orderList.get(i).getUnitPrice());
                listOrder.setDiscount(orderList.get(i).getDiscount());
                dao.update(listOrder);
            }
        }
    }
    private void updateCash(){
        OrderDao dao = new OrderDao();
        orderList = dao.getAllList();
        cashList = dao.getAllCash();
        orderList2 = dao.getAll();
        int ordersize = orderList2.size() - 1;
        for (int i = 0; i < cashList.size(); i++) {
                if(cashList.get(i).getOrderId()==0){
                    Change c = new Change(0,0,0);
                    c.setOrderId(orderList2.get(ordersize).getId());
                    c.setChange(cashList.get(i).getChange());
                    c.setCash(cashList.get(i).getCash());
                    dao.update(c);
                }
            }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        posPanel = new javax.swing.JPanel();
        llblBill = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblPrice = new javax.swing.JLabel();
        lblList = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblCash = new javax.swing.JLabel();
        lblchange = new javax.swing.JLabel();
        lblSpaceTotal = new javax.swing.JLabel();
        lblSpaceChange = new javax.swing.JLabel();
        lblSpaceCash = new javax.swing.JLabel();
        Jtbl = new javax.swing.JScrollPane();
        tblList = new javax.swing.JTable();
        inputCash = new javax.swing.JTextField();
        btnCal = new javax.swing.JButton();
        lblAmount = new javax.swing.JLabel();
        lblSpaceAmount = new javax.swing.JLabel();
        btnBill = new javax.swing.JButton();
        lblDiscount = new javax.swing.JLabel();
        inputMember = new javax.swing.JTextField();
        btnMember = new javax.swing.JButton();
        lblSpaceDiscount = new javax.swing.JLabel();
        lblIncash = new javax.swing.JLabel();
        btnNewOrder = new javax.swing.JButton();
        lblMember = new javax.swing.JLabel();
        btnHot = new javax.swing.JButton();
        btnIce = new javax.swing.JButton();
        scrReceipt = new javax.swing.JScrollPane();

        setBackground(new java.awt.Color(255, 204, 102));
        setPreferredSize(new java.awt.Dimension(941, 603));
        setRequestFocusEnabled(false);
        setVerifyInputWhenFocusTarget(false);

        jScrollPane2.setMaximumSize(new java.awt.Dimension(500, 500));
        jScrollPane2.setMinimumSize(new java.awt.Dimension(500, 500));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(500, 500));

        posPanel.setLayout(new java.awt.GridLayout(1, 0));
        jScrollPane2.setViewportView(posPanel);

        llblBill.setBackground(new java.awt.Color(204, 204, 204));

        lblName.setBackground(new java.awt.Color(255, 102, 0));
        lblName.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblName.setText("Calculator");

        lblPrice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPrice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPrice.setText("Price");

        lblList.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblList.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblList.setText("List");

        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTotal.setText("Total");

        lblCash.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCash.setText("cash");

        lblchange.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblchange.setText("change");

        lblSpaceTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblSpaceChange.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblSpaceCash.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        tblList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Jtbl.setViewportView(tblList);

        inputCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputCashActionPerformed(evt);
            }
        });

        btnCal.setBackground(new java.awt.Color(255, 102, 0));
        btnCal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCal.setText("Calculate");
        btnCal.setActionCommand("");
        btnCal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalActionPerformed(evt);
            }
        });

        lblAmount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAmount.setText("Amount");
        lblAmount.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                lblAmountAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        lblSpaceAmount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSpaceAmount.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                lblSpaceAmountAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        btnBill.setBackground(new java.awt.Color(255, 102, 0));
        btnBill.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBill.setText("Bill");
        btnBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBillActionPerformed(evt);
            }
        });

        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDiscount.setText("discount");

        inputMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputMemberActionPerformed(evt);
            }
        });

        btnMember.setBackground(new java.awt.Color(255, 102, 0));
        btnMember.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnMember.setText("Member");
        btnMember.setActionCommand("");
        btnMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberActionPerformed(evt);
            }
        });

        lblSpaceDiscount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblIncash.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblIncash.setText("Cash:");

        btnNewOrder.setBackground(new java.awt.Color(255, 102, 0));
        btnNewOrder.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnNewOrder.setText("New order");
        btnNewOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewOrderActionPerformed(evt);
            }
        });

        lblMember.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMember.setText("Member:");

        javax.swing.GroupLayout llblBillLayout = new javax.swing.GroupLayout(llblBill);
        llblBill.setLayout(llblBillLayout);
        llblBillLayout.setHorizontalGroup(
            llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, llblBillLayout.createSequentialGroup()
                .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(llblBillLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMember, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(inputMember, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMember, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .addComponent(btnNewOrder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(llblBillLayout.createSequentialGroup()
                                .addComponent(lblIncash, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(inputCash, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnBill, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCal, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(llblBillLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(llblBillLayout.createSequentialGroup()
                                .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblchange, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblCash, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 246, Short.MAX_VALUE)
                                .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSpaceChange, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSpaceCash, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSpaceTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSpaceDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSpaceAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(llblBillLayout.createSequentialGroup()
                                .addComponent(lblList, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(Jtbl))))
                .addGap(22, 22, 22))
        );
        llblBillLayout.setVerticalGroup(
            llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(llblBillLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblList, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(Jtbl, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(llblBillLayout.createSequentialGroup()
                        .addComponent(lblAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblCash, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblchange, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(llblBillLayout.createSequentialGroup()
                        .addComponent(lblSpaceAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, llblBillLayout.createSequentialGroup()
                                .addComponent(lblSpaceCash, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblSpaceChange, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, llblBillLayout.createSequentialGroup()
                                .addComponent(lblSpaceTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(65, 65, 65)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblSpaceDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(llblBillLayout.createSequentialGroup()
                        .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(inputCash, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblIncash, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCal))
                    .addGroup(llblBillLayout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(btnMember))
                    .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(inputMember, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblMember, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(llblBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBill)
                    .addComponent(btnNewOrder))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnHot.setBackground(new java.awt.Color(255, 102, 0));
        btnHot.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        btnHot.setText("Hot");
        btnHot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHotActionPerformed(evt);
            }
        });

        btnIce.setBackground(new java.awt.Color(255, 102, 0));
        btnIce.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        btnIce.setText("Ice");
        btnIce.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnHot, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(btnIce, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(28, 28, 28)
                .addComponent(llblBill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrReceipt, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnHot)
                    .addComponent(btnIce))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(llblBill, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(scrReceipt, javax.swing.GroupLayout.Alignment.TRAILING)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnHotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHotActionPerformed
        getType("hot");
    }//GEN-LAST:event_btnHotActionPerformed

    private void btnIceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIceActionPerformed
        getType("ice");
    }//GEN-LAST:event_btnIceActionPerformed

    private void inputCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputCashActionPerformed

    }//GEN-LAST:event_inputCashActionPerformed

    private void btnCalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalActionPerformed
        lblSpaceCash.setText(inputCash.getText());
        cal();
    }//GEN-LAST:event_btnCalActionPerformed

    private void lblAmountAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_lblAmountAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_lblAmountAncestorAdded

    private void lblSpaceAmountAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_lblSpaceAmountAncestorAdded

    }//GEN-LAST:event_lblSpaceAmountAncestorAdded

    private void btnBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBillActionPerformed
        addOrderToFrom();
        scrReceipt.setViewportView(new ReceiptPanel());
        list.reset();
        updateIdOrderList();
        updateCash();
        
    }//GEN-LAST:event_btnBillActionPerformed

    private void inputMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputMemberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inputMemberActionPerformed

    private void btnMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberActionPerformed
        checkMember();
        discount();
    }//GEN-LAST:event_btnMemberActionPerformed

    private void btnNewOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewOrderActionPerformed
        initForm();
        loadProduct();
    }//GEN-LAST:event_btnNewOrderActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane Jtbl;
    private javax.swing.JButton btnBill;
    private javax.swing.JButton btnCal;
    private javax.swing.JButton btnHot;
    private javax.swing.JButton btnIce;
    private javax.swing.JButton btnMember;
    private javax.swing.JButton btnNewOrder;
    private javax.swing.JTextField inputCash;
    private javax.swing.JTextField inputMember;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblAmount;
    private javax.swing.JLabel lblCash;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblIncash;
    private javax.swing.JLabel lblList;
    private javax.swing.JLabel lblMember;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblSpaceAmount;
    private javax.swing.JLabel lblSpaceCash;
    private javax.swing.JLabel lblSpaceChange;
    private javax.swing.JLabel lblSpaceDiscount;
    private javax.swing.JLabel lblSpaceTotal;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblchange;
    private javax.swing.JPanel llblBill;
    private javax.swing.JPanel posPanel;
    private javax.swing.JScrollPane scrReceipt;
    private javax.swing.JTable tblList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(ProductImage product, int amount) {
        double total = product.getPrice() * amount;
        OrderDao dao = new OrderDao();
        orderList = dao.getAllList();
        if (amount != 0) {
            OrderDetail listOrder = new OrderDetail(0, 0, 0, 0.0, 0.0);
            listOrder.setProId(product.getId());
            listOrder.setTotal(total);
            listOrder.setUnitPrice(product.getPrice());
            dao.addList(listOrder);
            System.out.println(listOrder.toString());
        } else {
            for (int i = 0; i < orderList.size(); i++) {
                if (orderList.get(i).getOrderId() == 0 & orderList.get(i).getProId() == product.getId()) {
                    OrderDetail listOrder = new OrderDetail(0, 0, 0, 0.0, 0.0);
                    //listOrder.setOrderId(orderList2.get(ordersize).getId());
                    listOrder.setId(orderList.get(i).getId());
                    listOrder.setProId(orderList.get(i).getProId());
                    listOrder.setTotal(orderList.get(i).getTotal());
                    listOrder.setUnitPrice(orderList.get(i).getUnitPrice());
                    listOrder.setDiscount(10);
                    dao.update(listOrder);
                    dao.deleteListOrder(10);
                }
            }
        }
        
        if (list == null) {
            list = new OrderPos(0, null, 0.0, null, 0.0, null);
            list.addOrder(product, amount);

        } else {
            list.addOrder(product, amount);
        }
        refershTable();
        loadTable();
        loadTotal();
    }

    private class ListTableModel extends AbstractTableModel {

        private final ArrayList<OrderList> data;
        String[] columnName = {"name", "amount", "price"};

        public ListTableModel(ArrayList<OrderList> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            if (data == null) {
                return 0;
            }
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            OrderList product = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return product.getProduct().getName();
            }
            if (columnIndex == 1) {
                return product.getAmount();
            }
            if (columnIndex == 2) {
                return product.getAmount() * product.getProduct().getPrice();
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columnName[column];
        }

    }
}
