/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Database.Database;
import Piyachat_61160263.Change;
import Piyachat_61160263.OrderDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;

/**
 *
 * @author Jirapat
 */
public class OrderDao implements DaoInterface<Order>{

    @Override
    public int add(Order object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO [order] (cus_id,emp_id,name,price,date) VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCus_id());
            stmt.setInt(2, object.getEmp_id());
            stmt.setString(3, object.getName());
            stmt.setDouble(4, object.getPrice());
            stmt.setString(5, object.getDate());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;

    }

    @Override
    public ArrayList<Order> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT id,cus_id,emp_id,name,price,date FROM [order]";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int cus_id = result.getInt("cus_id");
                int emp_id = result.getInt("emp_id");
                String name = result.getString("name");
                Double price = result.getDouble("price");
                String date = result.getString("date");
                Order order = new Order(id, cus_id, emp_id, name, price, date);
                list.add(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Order get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,price,date FROM [order] WHERE id = " + id;
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            if (result.next()) {
                int oid = result.getInt("id");
                int cus_id = result.getInt("cus_id");
                int emp_id = result.getInt("emp_id");
                String name = result.getString("name");
                Double price = result.getDouble("price");
                String date = result.getString("date");
                Order order = new Order(id, cus_id, emp_id, name, price, date);
                db.close();
                return order;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM [order] WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Order object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE [order] SET cus_id = ?, emp_id = ?, name = ?, price = ?, date = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCus_id());
            stmt.setInt(2, object.getEmp_id());
            stmt.setString(3, object.getName());
            stmt.setDouble(4, object.getPrice());
            stmt.setString(5, object.getDate());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    
    public int deleteListOrder(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM [order_detail] WHERE discount = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    
    public ArrayList<OrderDetail> getAllList() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT id,order_id,pro_id,total,unitprice,discount FROM [order_detail]";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int cus_id = result.getInt("order_id");
                int emp_id = result.getInt("pro_id");
                double total = result.getDouble("total");
                Double price = result.getDouble("unitprice");
                Double discount = result.getDouble("discount");
                OrderDetail order = new OrderDetail(id, cus_id, emp_id, total, price, discount);
                list.add(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    
    public int update(OrderDetail object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE [order_detail] SET order_id = ?, pro_id = ?, total = ?, unitprice = ?, discount = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getOrderId());
            stmt.setInt(2, object.getProId());
            stmt.setDouble(3, object.getTotal());
            stmt.setDouble(4, object.getUnitPrice());
            stmt.setDouble(5, object.getDiscount());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    
    public int addList(OrderDetail object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO order_detail (order_id,pro_id,total,unitprice,discount) VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getOrderId());
            stmt.setInt(2, object.getProId());
            stmt.setDouble(3, object.getTotal());
            stmt.setDouble(4, object.getUnitPrice());
            stmt.setDouble(5, object.getDiscount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;

    }
    
    public int add(Change object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO cash (order_id,cash,change) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getOrderId());
            stmt.setDouble(2, object.getCash());
            stmt.setDouble(3, object.getChange());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;

    }
    public int update(Change object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE [cash] SET cash = ?, change = ? WHERE order_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getOrderId());
            stmt.setDouble(2, object.getChange());
            stmt.setDouble(3, object.getCash());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    
    public ArrayList<Change> getAllCash() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT order_id,cash,change FROM [cash]";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("order_id");
                double cash = result.getInt("cash");
                double chnage = result.getInt("change");
                Change change = new Change(id, cash, chnage);
                list.add(change);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Order.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    
    public static void main(String[] args) {
        OrderDao dao = new OrderDao();
        System.out.println(dao.getAll());
        int id = dao.add(new Order(-1,2,2, "MKK", 0.0,""));
        
    }
    
}
