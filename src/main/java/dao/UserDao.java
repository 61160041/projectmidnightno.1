/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectUser;
import Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.User;

/**
 *
 * @author Jirapat
 */
public class UserDao implements DaoInterface<User> {

    private static String role;

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user (name ,lastname,tel,password,role,status)VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getPassword());
            stmt.setString(5, object.getRole());
            stmt.setString(6, object.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,lastname,tel,password,role,status FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String tel = result.getString("tel");
                String password = result.getString("password");
                String role = result.getString("role");
                String status = result.getString("status");
                User user = new User(id, name, lastname, tel, password, role, status);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,lastname,tel,password,role,status FROM user WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String tel = result.getString("tel");
                String password = result.getString("password");
                String role = result.getString("role");
                String status = result.getString("status");
                User user = new User(uid, name, lastname, tel, password, role, status);
                db.close();
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public User getByusername(String username) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,lastname,tel,password,role,status,emp_id FROM user WHERE name='" + username + "'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String tel = result.getString("tel");
                String password = result.getString("password");
                String role = result.getString("role");
                String status = result.getString("status");
                int emp_id = result.getInt("emp_id");
                User user = new User(uid, name, lastname, tel, password, role, status, emp_id);
                db.close();
                return user;
            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public ArrayList<User> search(String type) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,lastname,tel,password,role,status FROM user where name LIKE '%" + type + "%'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int uid = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String tel = result.getString("tel");
                String password = result.getString("password");
                String role = result.getString("role");
                String status = result.getString("status");
                User user = new User(uid, name, lastname, tel, password, role, status);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM user WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?,lastname = ?,tel = ?,password = ?,role = ?,status = ? WHERE  id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getPassword());
            stmt.setString(5, object.getRole());
            stmt.setString(6, object.getStatus());
            stmt.setInt(7, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    public static boolean auth(String loginName, String password) {
        boolean temp = false;
        UserDao dao = new UserDao();
        ArrayList<User> array = dao.getAll();
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).getName().equals(loginName) && array.get(i).getPassword().equals(password) && array.get(i).getStatus().equals("Working")) {
                role = array.get(i).getRole();
                System.out.println(role);
                temp = true;
                break;
            } else {
                temp = false;
            }
        }
        return temp;
    }

    public static String getRole() {
        return role;
    }

//    public static void main(String[] args) {
//        UserDao dao = new UserDao();
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(1));
//        int id = dao.add(new User(-1, "Dejavu", "jaidee", "0822455688", "password", "Employee", "Working"));
//        System.out.println("id: " + id);
//        System.out.println(dao.get(id));
//        User lastUser = dao.get(id);
//        System.out.println("list user: " + lastUser);
//        lastUser.setPassword("password1");
//        int row = dao.update(lastUser);
//        User updateUser = dao.get(id);
//        System.out.println("update user: " + updateUser);
//        dao.delete(id);
//        User deleteUser = dao.get(id);
//        System.out.println("delete user: " + deleteUser);
//
//    }

}
