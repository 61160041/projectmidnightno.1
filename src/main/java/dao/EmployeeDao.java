/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Database.Database;
import com.mycompany.storeproject.poc.TestSelectUser;
import model.Customer;

/**
 *
 * @author Jirapat
 */
public class EmployeeDao implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO employee (name,lastname,idcard,address,tel,salary) VALUES (?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLastname());
            stmt.setString(3, object.getIdcard());
            stmt.setString(4, object.getAddress());
            stmt.setString(5, object.getTel());
            stmt.setDouble(6, object.getSalary());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name,lastname,idcard,address,tel,salary FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String idcard = result.getString("idcard");
                String address = result.getString("address");
                String tel = result.getString("tel");
                double salary = result.getDouble("salary");
                Employee employee = new Employee(id, name, lastname, idcard, address, tel, salary);
               
                list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id, name,lastname,idcard,tel,address,salary FROM employee WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String idcard = result.getString("idcard");
                String address = result.getString("address");
                String tel = result.getString("tel");
                double salary = result.getDouble("salary");
                Employee employee = new Employee(id, name, lastname, idcard, address, tel, salary);
                db.close();
                return employee;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM employee WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public String getName(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        String out = "Unknow";
        try {
            String sql = "SELECT id, name,lastname,idcard,tel,address,salary FROM employee WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String idcard = result.getString("idcard");
                String address = result.getString("address");
                String tel = result.getString("tel");
                double salary = result.getDouble("salary");
                Employee employee = new Employee(id, name, lastname, idcard, address, tel, salary);
                out = employee.getName();
                db.close();
                return out;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return out;
    }
    public String getLastName(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        String out = "Unknow";
        try {
            String sql = "SELECT id, name,lastname,idcard,tel,address,salary FROM employee WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String idcard = result.getString("idcard");
                String address = result.getString("address");
                String tel = result.getString("tel");
                double salary = result.getDouble("salary");
                Employee employee = new Employee(id, name, lastname, idcard, address, tel, salary);
                out = employee.getLastname();
                db.close();
                return out;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return out;
    }


    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE employee SET name = ?,lastname = ?,idcard = ?,address = ?,tel = ?,salary = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLastname());
            stmt.setString(3, object.getIdcard());
            stmt.setString(4, object.getAddress());
            stmt.setString(5, object.getTel());
            stmt.setDouble(6, object.getSalary());
            stmt.setInt(7, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Employee(-1, "Gunnn", "Ratchanon", "1200901269044", "Chonburi", "0877566544", 1200));
        System.out.println("id: " + id);
        System.out.println(dao.get(id));
        Employee lastEmployee = dao.get(id);
        System.out.println("list employee: " + lastEmployee);
        lastEmployee.setLastname("Deejai");
        int row = dao.update(lastEmployee);
        Employee updateEmployee = dao.get(id);
        System.out.println("update employee: " + updateEmployee);
        dao.delete(id);
        Employee deleteEmployee = dao.get(id);
        System.out.println("delete employee: " + deleteEmployee);

    }

    public ArrayList<Employee> search(String in) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id, name,lastname,idcard,address,tel,salary FROM employee where name LIKE '%" + in + "%'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String lastname = result.getString("lastname");
                String idcard = result.getString("idcard");
                String address = result.getString("address");
                String tel = result.getString("tel");
                int salary = result.getInt("salary");

                Employee employee = new Employee(id, name, lastname, idcard, address, tel, salary);
                list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

}
