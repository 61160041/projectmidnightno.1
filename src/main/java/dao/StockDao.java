/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Stock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Database.Database;
import com.mycompany.storeproject.poc.TestSelectUser;

/**
 *
 * @author Jirapat
 */
public class StockDao implements DaoInterface<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO stock (name,type,amount) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getType());
            stmt.setInt(3, object.getAmount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,type,amount FROM stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String type = result.getString("type");
                int amount = result.getInt("amount");
                Stock stock = new Stock(id, name, type, amount);
                list.add(stock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,type,amount FROM stock WHERE id = " + id;
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            if (result.next()) {
                int sid = result.getInt("id");
                String name = result.getString("name");
                String type = result.getString("type");
                int amount = result.getInt("amount");
                Stock stock = new Stock(sid, name, type, amount);
                db.close();
                return stock;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public ArrayList<Stock> search(String in) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,type,amount FROM stock WHERE name LIKE '%"+in+"%'" ;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int sid = result.getInt("id");
                String name = result.getString("name");
                String type = result.getString("type");
                int amount = result.getInt("amount");
                Stock stock = new Stock(sid, name, type, amount);
                list.add(stock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM stock WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE stock SET name = ?, type = ?, amount = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getType());
            stmt.setInt(3, object.getAmount());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        StockDao dao = new StockDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Stock(-1, "CoffeeBean", "ingredient", 10));
        System.out.println("id: " + id);
        System.out.println(dao.get(id));
        Stock lastStock = dao.get(id);
        System.out.println("list stock: " + lastStock);
        lastStock.setAmount(15);
        int row = dao.update(lastStock);
        Stock updateStock = dao.get(id);
        System.out.println("update stock: " + updateStock);
        dao.delete(id);
        Stock deleteStock = dao.get(id);
        System.out.println("delete stock: " + deleteStock);
    }

}
