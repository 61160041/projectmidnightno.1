/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Database.Database;
import com.mycompany.storeproject.poc.TestSelectProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;
import model.TimeCheck;

/**
 *
 * @author ADMIN
 */
public class TimeCheckDao implements DaoInterface<TimeCheck> {

    @Override
    public int add(TimeCheck object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int id = -1;
        try {
            String sql = "INSERT INTO time_check (emp_id,date,in_time,out_time)VALUES (?,?,?,?)";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getEmpId());
            stmt.setString(2, object.getDate());
            stmt.setString(3, object.getInTime());
            stmt.setString(4, "0");
            stmt.executeUpdate();
            result = stmt.getGeneratedKeys();
            stmt.close();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("erorr" + ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<TimeCheck> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,emp_id,date,in_time,out_time FROM time_check";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int empid = result.getInt("emp_id");
                String date = result.getString("date");
                String intime = result.getString("in_time");
                String outtime = result.getString("out_time");

                TimeCheck timecheck = new TimeCheck(id, empid, date, intime, outtime);
                list.add(timecheck);
            }
        } catch (SQLException ex) {
            // Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public TimeCheck get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,emp_id,date,in_time,out_time FROM time_check WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                int eid = result.getInt("emp_id");
                String date = result.getString("date");
                String intime = result.getString("in_time");
                String outtime = result.getString("out_time");

                TimeCheck timecheck = new TimeCheck(pid, eid, intime, outtime, date);
                db.close();

                return timecheck;
            }
        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM time_check WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(TimeCheck object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE time_check SET  emp_id = '?', date = '?', in_time = '?', out_time = '?' WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getEmpId());
            stmt.setString(2, object.getDate());
            stmt.setString(3, object.getInTime());
            stmt.setString(4, object.getOutTime());
            stmt.setInt(5, object.getId());

            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TimeCheck.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public int updateOutTime(String date, int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE time_check SET out_time =" + "\"" + date + "\"" + " WHERE id =" + id;
            PreparedStatement stmt = conn.prepareStatement(sql);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TimeCheck.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        TimeCheckDao dao = new TimeCheckDao();
        System.out.println(dao.get(1));
//        dao.add(new TimeCheck(1, "1111111", "222ASDAS222222", "ASDASDASD"));
//        System.out.println(in);

    }

}
