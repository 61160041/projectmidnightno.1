/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Database.Database;
import com.mycompany.storeproject.poc.TestSelectUser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.PaymentSalary;

/**
 *
 * @author ASUS
 */
public class PaymentSalaryDao implements DaoInterface<PaymentSalary> {

    public int add(PaymentSalary object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO payment_salary (emp_id,unit,total,date)VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getEmp_id());
            stmt.setDouble(2, object.getUnit());
            stmt.setDouble(3, object.getTotal());
            stmt.setString(4, object.getDate());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PaymentSalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<PaymentSalary> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,emp_id, unit,total,date FROM payment_salary ;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int emp_id = result.getInt("emp_id");
                Double unit = result.getDouble("unit");
                Double total = result.getDouble("total");
                String date = result.getString("date");
                PaymentSalary payment = new PaymentSalary(id, emp_id, unit, total, date);
                list.add(payment);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PaymentSalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public PaymentSalary get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,emp_id, unit,total,date FROM payment_salary WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                int emp_id = result.getInt("emp_id");
                Double unit = result.getDouble("unit");
                Double total = result.getDouble("total");
                String date = result.getString("date");
                PaymentSalary payment = new PaymentSalary(pid, emp_id, unit, total, date);
                db.close();
                return payment;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PaymentSalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM payment_salary WHERE id = ? ;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PaymentSalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(PaymentSalary object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE payment_salary SET emp_id = ?,unit = ?,total = ?,date = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,object.getEmp_id());
            stmt.setDouble(2, object.getUnit());
            stmt.setDouble(3, object.getTotal());
            stmt.setString(4, object.getDate());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PaymentSalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public Double getUnit(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        double out = 0;
        try {
            String sql = "SELECT id,emp_id, unit,total,date FROM payment_salary WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            if (result.next()) {
                int pid = result.getInt("id");
                int emp_id = result.getInt("emp_id");
                Double unit = result.getDouble("unit");
                Double total = result.getDouble("total");
                String date = result.getString("date");
                PaymentSalary payment = new PaymentSalary(pid, emp_id, unit, total, date);
                out = payment.getUnit();
                db.close();
                return out;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PaymentSalary.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return out;
    }
    
    public int getID(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int out = 0;
        try {
            String sql = "SELECT id,emp_id, unit,total,date FROM payment_salary WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            if (result.next()) {
                int pid = result.getInt("id");
                int emp_id = result.getInt("emp_id");
                Double unit = result.getDouble("unit");
                Double total = result.getDouble("total");
                String date = result.getString("date");
                PaymentSalary payment = new PaymentSalary(pid, emp_id, unit, total, date);
                out = payment.getId();
                db.close();
                return out;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PaymentSalary.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return out;
    }
    
    public static void main(String[] args) {
        PaymentSalaryDao dao = new PaymentSalaryDao();
        System.out.println(dao.getAll());
        int id = dao.add(new PaymentSalary(-1, 1, 0, 0, "-"));

    }
}
