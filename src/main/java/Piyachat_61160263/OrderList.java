/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Piyachat_61160263;

/**
 *
 * @author pet002
 */
public class OrderList {

    private int id;
    private ProductImage product;
    private int amount;
    private Double price;

    public OrderList(int id, ProductImage product, int amount) {
        this.id = id;
        this.product = product;
        this.amount = amount;
    }

    public OrderList(ProductImage product, int amount) {
        this(-1, product, amount);
    }

    public double getTotal() {
        return amount * product.getPrice();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductImage getProduct() {
        return product;
    }

    public void setProduct(ProductImage product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void addAmount(int amount){
        this.amount = this.amount+amount;
    }
    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", product=" + product + ", amount=" + amount + ", price=" + price + '}';
    }

}
