/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Piyachat_61160263;

import java.util.ArrayList;

/**
 *
 * @author pet002
 */
public class OrderPos {

    private int id;
    private String name;
    private Double price;
    private String type;
    private Double amount;
    private String image;
    private int amount1;

    private ArrayList<OrderList> itemList = new ArrayList<>();
    
    public OrderPos(int id, String name, Double price, String type, Double amount, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
        this.amount = amount;
        this.image = image;
    }

    public void addOrder(int id, ProductImage product, int amount1) {
        if(itemList.size()<=0 & amount1==0){
            return;
        }
        for (int row = 0; row < itemList.size(); row++) {
            OrderList i = itemList.get(row);
            if(i.getProduct().getId() == product.getId() & amount1==0){
                itemList.remove(i);
                System.out.println(id);
                return;
            }else
            if (i.getProduct().getId() == product.getId()) {
                i.addAmount(amount1);
                return;
            }
        }
        itemList.add(new OrderList(id, product, amount1));
        System.out.println(itemList.size());
    }

    public void addOrder(ProductImage product, int amount1) {
        addOrder(-1, product, amount1);

    }

    public double Total() {
        double total = 0.0;
        for (OrderList p : itemList) {
            total = total + p.getTotal();
        }
        return total;
    }

    public double allAmount() {
        int amount = 0;
        for (OrderList p : itemList) {
            amount = amount + p.getAmount();
        }
        return amount;
    }

    public double resetAmount() {
        int amount = 0;
        return amount;
    }
    
    public ArrayList<OrderList> reset(){
        itemList.clear();
        return itemList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getAmount1() {
        return amount1;
    }

    public void setAmount1(int amount1) {
        this.amount1 = amount1;
    }

    public ArrayList<OrderList> getData() {
        
        return itemList;
    }

    public void setData(ArrayList<OrderList> itemList) {
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        return "ProductAmount{" + "id=" + id + ", name=" + name + ", price=" + price + ", type=" + type + ", amount=" + amount + ", image=" + image + ", amount1=" + amount1 + '}';
    }

}
