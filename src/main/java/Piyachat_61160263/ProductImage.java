/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Piyachat_61160263;

import java.util.ArrayList;

/**
 *
 * @author pet00
 */
public class ProductImage {

    private int id;
    private String name;
    private double price;
    private String type;
    private double amount;
    private String image;

    public ProductImage(int id, String name, Double price, String type, Double amount, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
        this.amount = amount;
        this.image = image;
    }

    public ProductImage(String name, Double price, String type, Double amount, String image) {
        this(-1, name, price, type, amount, image);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ProductImage{" + "id=" + id + ", name=" + name + ", "
                + "price=" + price + ", type=" + type + ", amount=" + amount
                + ", image=" + image + '}';
    }

    public static ArrayList<ProductImage> genProductList() {
        ArrayList<ProductImage> list = new ArrayList<>();
        /*list.add(new ProductImage(1, "caramel late", 35, "-", 20,"1.jpg"));
        list.add(new ProductImage(2, "late", 30, "-", 12,"2.jpg"));
        list.add(new ProductImage(3, "cappuccino", 35, "-", 30,"3.jpg"));
        list.add(new ProductImage(6, "americano", 35, "-", 10,"4.jpg"));
        list.add(new ProductImage(7, "coffee2", 20, "-", 15,"5.jpg"));
        list.add(new ProductImage(8, "cofee3", 45, "-", 50,"6.jpg"));
        list.add(new ProductImage(11, "cofee4", 45, "-", 20,"7.jpg"));
        list.add(new ProductImage(12, "coco", 50, "-", 20,"8.jpg"));*/
        return list;
    }

}
