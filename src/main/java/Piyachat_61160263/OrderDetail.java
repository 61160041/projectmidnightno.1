/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Piyachat_61160263;

/**
 *
 * @author pet002
 */
public class OrderDetail {
    private int id;
    private int orderId;
    private int proId;
    private double total;
    private double unitPrice;
    private double discount;

    public OrderDetail(int id, int orderId, int proId, double total, double unitPrice, double discount) {
        this.id = id;
        this.orderId = orderId;
        this.proId = proId;
        this.total = total;
        this.unitPrice = unitPrice;
        this.discount = discount;
    }
    
    public OrderDetail(int orderId, int proId, double total, double unitPrice, double discount){
        this(-1,orderId,proId,total,unitPrice,discount);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "id=" + id + ", orderId=" + orderId + ", proId=" + proId + ", total=" + total + ", unitPrice=" + unitPrice + ", discount=" + discount + '}';
    }
    
    
}
