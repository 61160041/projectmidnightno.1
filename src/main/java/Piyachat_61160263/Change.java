/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Piyachat_61160263;

/**
 *
 * @author pet002
 */
public class Change {
    private int orderId;
    private double change;
    private double cash;

    public Change(int orderId, double change,double cash) {
        this.orderId = orderId;
        this.change = change;
        this.cash = cash;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    @Override
    public String toString() {
        return "Change{" + "change=" + change + ", total=" + cash + '}';
    }

    
    
    
}
