/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Piyachat_61160263;

/**
 *
 * @author pet002
 */
public class ReceiptDetail {
    private String name;
    private double total;

    public ReceiptDetail(String name, double total) {
        this.name = name;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "name=" + name + ", total=" + total + '}';
    }
    
    
}
