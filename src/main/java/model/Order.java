/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;


/**
 *
 * @author Jirapat
 */
public class Order {
    Date day = new Date();
    private int id;
    private int cus_id;
    private int emp_id;
    private String name;
    private Double price;
    private String date;

    public Order(int id, int cus_id, int emp_id, String name, Double price, String date) {
        this.id = id;
        this.cus_id = cus_id;
        this.emp_id = emp_id;
        this.name = name;
        this.price = price;
        this.date = date;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCus_id() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Order{" + "day=" + day + ", id=" + id + ", cus_id=" + cus_id + ", emp_id=" + emp_id + ", name=" + name + ", price=" + price + ", date=" + date + '}';
    }
    
    
}
