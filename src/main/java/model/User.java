/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Jirapat
 */
public class User {
    private int id;
    private String name;
    private String lastname;
    private String tel;
    private String password;
    private String role;
    private String status;
    private int emp_id;

    public User(int id, String name, String lastname, String tel, String password, String role, String status) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.tel = tel;
        this.password = password;
        this.role = role;
        this.status = status;
    }
    
    public User(int id, String name, String lastname, String tel, String password, String role, String status,int emp_id) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.tel = tel;
        this.password = password;
        this.role = role;
        this.status = status;
        this.emp_id = emp_id;
    }

    public User(int id, String name, String tel) {
         this(-1,name,"",tel,"","","");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
   
    public void setName(String name) {
        this.name = name;
    }
    
    public String getLastName() {
        return lastname;
    }
    
    public void setLastName(String lastname){
        this.lastname = lastname;
    }
    
    public String getTel() {
        return tel;
    }
    
    public int getEmpID() {
        return emp_id;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", lastname="+ lastname +", tel=" + tel + ", password=" + password + '}';
    }

}
