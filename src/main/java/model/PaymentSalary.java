/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author ASUS
 */
public class PaymentSalary {
    Date today = new Date();
    private int id;
    private int emp_id;
    private double unit;
    private double total;
    private String date;

    public PaymentSalary(int id, int emp_id, double unit, double total, String date) {
        this.id = id;
        this.emp_id = emp_id;
        this.unit = unit;
        this.total = total;
        this.date = date;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public double getUnit() {
        return unit;
    }

    public void setUnit(double unit) {
        this.unit = unit;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "PaymentSalary{" + "today=" + today + ", id=" + id + ", emp_id=" + emp_id + ", unit=" + unit + ", total=" + total + ", date=" + date + '}';
    }
    
    
}
