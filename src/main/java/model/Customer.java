/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Jirapat
 */
public class Customer {
    private int id;
    private String name;
    private String tel;
    private String lastname;
    private String status;
    private String address;

    public Customer(int id, String name, String lastname, String tel, String status, String address) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.lastname = lastname;
        this.status = status;
        this.address = address;
    }

    public Customer(int id, String name, String tel) {
      this(-1,name,tel,"","","");
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", lastname=" + lastname + ", status=" + status + ", address=" + address + '}';
    }

    
}
