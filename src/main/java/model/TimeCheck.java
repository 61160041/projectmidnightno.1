/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author ADMIN
 */
public class TimeCheck {

    Date today = new Date();
    private int id;
    private int empId;
    private String date;
    private String inTime;
    private String outTime;

    public TimeCheck(int id, int emp_id, String date, String in_time, String out_time) {
        this.id = id;
        this.empId = emp_id;
        this.date = date;
        this.inTime = in_time;
        this.outTime = out_time;

    }

    public TimeCheck(int emp_id, String date, String in_time, String out_time) {
        this.empId = emp_id;
        this.date = date;
        this.inTime = in_time;
        this.outTime = out_time;

    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    @Override
    public String toString() {
        return "TimeCheck{" + "today=" + today + ", id=" + id + ", empId=" + empId + ", date=" + date + ", inTime=" + inTime + ", outTime=" + outTime + '}';
    }

}
